import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Person } from '../people/person.model';

@Component({
  selector: 'app-big-form',
  templateUrl: './big-form.component.html',
  styleUrls: ['./big-form.component.css']
})
export class BigFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: Person;
  @Output() formSubmitted = new EventEmitter<Person>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      firstName: this.data.firstName,
      lastName: this.data.lastName
    });
  }

  onFormSubmit() {
    this.formSubmitted.emit(this.form.value);
  }

  formInitialized(name: string, subForm: FormGroup) {
    this.form.setControl(name, subForm);
  }
}
