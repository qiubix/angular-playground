export interface Person {
  id: number;
  firstName: string;
  lastName: string;
  address: Address;
}

export interface Address {
  city: string;
  street: string;
}
