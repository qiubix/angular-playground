import { Injectable } from '@angular/core';
import { Person } from './person.model';
import { Observable, of, BehaviorSubject } from 'rxjs';

const ALL_PEOPLE: Person[] = [
  { id: 1, firstName: 'Joe', lastName: 'Doe', address: { city: '', street: '' } },
  { id: 2, firstName: 'Jane', lastName: 'Foster', address: { city: '', street: '' } },
  { id: 3, firstName: 'Jake', lastName: 'Foster', address: { city: '', street: '' } }
];

@Injectable()
export class PeopleService {
  allPeople$ = new BehaviorSubject(ALL_PEOPLE);

  private nextId = 4;

  constructor() {}

  getAll(): Observable<Person[]> {
    return this.allPeople$.asObservable();
  }

  addNew(person: Person): Observable<Person> {
    // console.log('service - new person added: ', person);
    const newPerson = person;
    newPerson.id = this.nextId++;
    this.allPeople$.next([...this.allPeople$.value, newPerson]);
    return of(newPerson);
  }

  update(updatedPerson: Person): Observable<Person> {
    // console.log('updatedPerson: ', updatedPerson);
    this.allPeople$.next([
      ...this.allPeople$.value.map(it => (it.id === updatedPerson.id ? updatedPerson : it))
    ]);
    return of(updatedPerson);
  }
}
