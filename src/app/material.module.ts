import { NgModule } from '@angular/core';
import {
  MatTableModule,
  MatCardModule,
  MatDialogModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatDividerModule
} from '@angular/material';

@NgModule({
  exports: [
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule
  ]
})
export class MaterialModule {}
