import { Component, OnInit, OnDestroy } from '@angular/core';
import { PeopleService } from '../people/people.service';
import { Person } from '../people/person.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-simple-dashboard',
  templateUrl: './simple-dashboard.component.html',
  styleUrls: ['./simple-dashboard.component.css']
})
export class SimpleDashboardComponent implements OnInit {
  constructor(private peopleService: PeopleService) {}

  people: Observable<Person[]>;

  ngOnInit() {
    this.people = this.peopleService.getAll();
  }

  onAddNewPerson(newPerson: Person) {
    this.peopleService.addNew(newPerson);
  }

  onEdit(updatedPerson: Person) {
    this.peopleService.update(updatedPerson);
  }
}
