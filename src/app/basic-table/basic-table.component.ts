import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../people/person.model';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { AddNewEntryDialogComponent } from '../add-new-dialog/add-new-dialog.component';

@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.css']
})
export class BasicTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'city', 'options'];

  dataSource: MatTableDataSource<Person> = new MatTableDataSource<Person>();

  @Input()
  set people(people: Person[]) {
    this.dataSource.data = people;
  }

  @Output() addNewEvent = new EventEmitter<Person>();
  @Output() editEvent = new EventEmitter<Person>();

  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  openAddNewDialog() {
    const emptyPerson: Person = {
      id: null,
      firstName: null,
      lastName: null,
      address: {
        city: null,
        street: null
      }
    };
    const dialogRef = this.dialog.open(AddNewEntryDialogComponent, { data: emptyPerson });
    dialogRef.afterClosed().subscribe((result: Person) => {
      if (result) {
        this.addNewEvent.emit(result);
      }
    });
  }

  editPerson(person: Person) {
    const dialogRef = this.dialog.open(AddNewEntryDialogComponent, { data: person });
    dialogRef.afterClosed().subscribe((result: Person) => {
      if (result) {
        result.id = person.id;
        this.editEvent.emit(result);
      }
    });
  }
}
