import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Person } from '../people/person.model';

@Component({
  selector: 'app-add-new-dialog',
  templateUrl: './add-new-dialog.component.html',
  styleUrls: ['./add-new-dialog.component.css']
})
export class AddNewEntryDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AddNewEntryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Person
  ) {}

  ngOnInit() {}

  onFormSubmitted(data: Person) {
    // console.log('Form submitted: ', data);
    this.dialogRef.close(data);
  }
}
