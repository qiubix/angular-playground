import { NgModule, SimpleChange } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { BasicTableComponent } from './basic-table/basic-table.component';
import { BigFormComponent } from './big-form/big-form.component';
import { SimpleDashboardComponent } from './simple-dashboard/simple-dashboard.component';
import { PeopleService } from './people/people.service';
import { AddNewEntryDialogComponent } from './add-new-dialog/add-new-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { AddressFormComponent } from './big-form/address-form/address-form.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleDashboardComponent,
    BasicTableComponent,
    BigFormComponent,
    AddressFormComponent,
    AddNewEntryDialogComponent
  ],
  imports: [BrowserModule, MaterialModule, BrowserAnimationsModule, ReactiveFormsModule],
  providers: [PeopleService],
  entryComponents: [AddNewEntryDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
